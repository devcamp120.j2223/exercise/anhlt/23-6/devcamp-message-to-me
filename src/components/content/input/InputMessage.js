import { Component } from "react";

class InputMessage extends Component {
    inputchangeHandle(event){
       console.log("input vừa nhập");
       console.log(event.target.value);
    }
    BtnHanderOnClick(){
        console.log("nut click vừa bấm");

    }
    render() {
        return (
            <div>
                <div className="row mt-2">
                    <div className="col-12">
                        <label className="form-label">Message cho bạn 12 tháng tới:</label>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <input className="form-control" placeholder="Nhập message" onChange={this.inputchangeHandle}/>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-12">
                        <button className="btn btn-success" onClick={this.BtnHanderOnClick}>Gửi thông điệp</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default InputMessage;